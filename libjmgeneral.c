﻿/*
Copyright <2024> <Jason Gibson>

<BSD License 2.0>

Redistribution and use in source and binary forms, 
with or without modification, are permitted provided 
that the following conditions are met:

1. Redistributions of source code must retain the above 
copyright notice, this list of conditions and the 
following disclaimer. 

2. Redistributions in binary form must reproduce the 
above copyright notice, this list of conditions and 
the following disclaimer in the documentation and/or 
other materials provided with the distribution. 

3. Neither the name of the copyright 
holder nor the names of its contributors may be used 
to endorse or promote products derived from this 
software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS 
AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED 
WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE 
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR 
A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL 
THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY 
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR 
CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, 
PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF 
USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) 
HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER
IN CONTRACT, STRICT LIABILITY, OR TORT 
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY 
OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF 
THE POSSIBILITY OF SUCH DAMAGE.
*/

#include <stdio.h>
#include <stdbool.h>
#include <stdlib.h>
#include <stdarg.h>
#include <string.h>
#include <assert.h>
#include <math.h>
#include <time.h>
#include <ctype.h>
#include <pthread.h>
#include <unistd.h>
#include <sys/socket.h>
#include <sodium.h>
#include "jmgeneral.h"

typedef struct mainstruct {
   int a;
   int b;
} JMGEN_STRUCT;

/*
  clear terminal output
*/

int
clear_stdout(void)
{
   int retval = 0;
   // added return check to fix unused result error in gcc
   if (system("clear") == 0) {
      retval = 1;
   }
   return(retval);
}

/*
  prompt for input with variable message
  ARGS:
    prompt = message for prompt
    a = size of buffer
*/

char *
prompt_input_getline(const char * prompt)
{
   assert(prompt);
  
   // declare & initialize
   char * retval = NULL;

   // print & get input
   prompt_print(prompt);
   retval = getline_stdin_mem_alloc();
   retval[strcspn(retval,
                  "\n")] = '\0';

   return(retval);
}

void
prompt_print(const char * prompt)
{
   assert(prompt);
   printf("%s --> ",
          prompt);
}

/*
  prompt to validate input
*/

bool
valid_input(char * str) // working
{
   assert(str);
   // declare & initialize
   char * temp = NULL;
   bool retval = false;

   // get input from user
   printf("you entered %s. is this correct? (yes|no) --> ",
          str);
   temp = getline_stdin_mem_alloc();

   // check first character of input for yes indicator
   if ((temp[0] == 'Y' ||
        temp[0] == 'y') &&
       (temp[1] == 'E' ||
        temp[1] == 'e') &&
       (temp[2] == 'S' ||
        temp[2] == 's')) {
      retval = true;
   }

   // clean up & return
   free(temp); temp = NULL;
   return(retval);
}

char *
home_file_paths(void)
{
   char * retval = NULL;
   retval = strdup(getenv("HOME"));
   return(retval);
}

void
ret_nano_secs(void * arg)
{
   JMGEN_STRUCT * localst = (JMGEN_STRUCT *)arg;
   struct timespec now;
   clock_gettime(CLOCK_REALTIME, &now);
   localst->a = (unsigned int) now.tv_nsec;
}

/*
  seeds srand with nanoseconds then returns random within specified range
  ARGS:
  low = minimum number to return
  high = max number to be generated
*/

int
random_gen(int high)
{
   srand(randombytes_random());
   return((unsigned int)rand() % high + 1);
}

/*
  reverses a string
  ARGS:
  a, b = *a, *b
*/

void
char_flip(char * a,
          char * b)
{
   assert(a);
   assert(b);
  
   char temp = *a;
   *a = *b;
   *b = temp;
}

/*
  flip 2 int variables
  ARGS:
  a, b = *a, *b
*/

void
int_flip(int * a,
         int * b)
{
   assert(a);
   assert(b);
  
   int temp = *a;
   *a = *b;
   *b = temp;
}

// general error printout & exit

void
prompt_fail(const char * fail,
            const char * func,
            int error)
{
   assert(fail);
   assert(func);
  
   printf("error: %s:%s:error - %d\n",
          fail,
          func,
          error);
   exit(1);
}

// https://stackoverflow.com/a/1069045

int
number_of_digits(long int a)
{
   // declare & initialize
   int b = 1;

   // count digits
   while (a /= 10) {
      b++;
   }

   return(b);
}

// get user input via getline

char *
getline_stdin_mem_alloc(void)
{
   // declare & initialize
   size_t buflen = 0;
   char * buffer = NULL;
   if (getline(&buffer,
               &buflen,
               stdin) == -1) {
     free(buffer);
     buffer = NULL;
   }
   buffer[strcspn(buffer,
                  "\n")] = '\0';

   return(buffer);
}

// converts binary string > decimal int

int
binary_to_decimal(char * num)
{
   assert(num);
   // declare & initialize
   int runningtotal = 0;
   char * buffer = strdup(num);

   // reverse string in buffer to run iterations
   string_flipper(buffer);

   // add up the running total as it iterates
   for (int i = 0; buffer[i] != '\0'; i++) {
      if (buffer[i] == '1') {
         runningtotal += (int)pow(2,
                                  i);
      }
   }

   // clean up & return
   free(buffer);
   buffer = NULL;
   return(runningtotal);
}

// converts decimal > binary char string
// creates string starting at the first set bit

char *
decimal_to_binary(long long int num)
{
   // declare & initialize
   char * retval = NULL;
   int retcount = 0;
   int retsize = 3;
  
   // loop long long size
   for (int i = 63; i >= 0; i--) {
      int j = num >> i;
      if (j & 1) {
         // allocate only when first bit found
         if (!retval) {
            retval = (char*)calloc(retsize, sizeof(char));
         }
         retval[retcount] = '1';
      } else {
         // skips initial zero bits until retval is allocated else sets 0
         if (!retval) {
            continue;
         } else {
            retval[retcount] = '0';
         }
      }
      // increment counters and realloc retval for next value
      if (retval) {
         retcount++;
         retsize++;
         retval = realloc(retval, 
                          retsize);
         // this line is remove a conditional jump error in valgrind
         retval[retcount] = '\0';
      }
   }
  
   return(retval);
}

char *
current_time(void)
{
   // declare & initialize
   char * retval = NULL;
   time_t localtime;

   time(&localtime);
   retval = strdup(ctime(&localtime));
   retval[strcspn(retval,
                  "\n")] = '\0';

   return(retval);
}

// open file for append / write depending on refresh bool value true / false
// fresh true will replace existing file with new. false will append

bool
file_create_refresh(FILE ** file,
                    char * name,
                    bool fresh)
{
   assert(file);
   assert(name);
  
   // declare & initialize
   bool retval = true;

   // open depending on refresh option
   if (fresh == true) {
      *file = fopen(name,
                    "w");
   } else {
      *file = fopen(name,
                    "a");
   }

  // return false for failure to open else true
   if (!file) {
      retval = false;
   }

   return(retval);
}

// close and null file pointers. fresh bool true will remove file after close
// false will leave file alone

bool
file_close_refresh(FILE ** file,
                   char * name,
                   bool fresh)
{
   assert(file);
   assert(name);
  
   // declare & initialize
   bool retval = true;

  // if file is set then close. option to remove is variable
   if (file) {
      fclose(*file);
      *file = NULL;
      if (fresh == true) {
         remove(name);
      }
   } else {
   // return false if file not exist
      retval = false;
   }

   // clean up and return true on success
   return(retval);
}

int
core_count(void)
{
   // declare & initialize
   int total_cores = 0;

   // open file for process
   FILE * file = fopen("/proc/stat",
                       "r");
   char * buffer = NULL;
   if (!file) {
      return(-1);
   }
   // create buffer to load each line from file into for processing
   while (!feof(file)) {
      if ((buffer = getline_mem_alloc(file))) {
         if (buffer[0] == 'c' &&
             buffer[1] == 'p' &&
             buffer[2] == 'u' &&
             isdigit((unsigned int)buffer[3])) {
            total_cores++;
         } else if (total_cores >= 1) {
            free(buffer);
            buffer = NULL;
            break;
         }
         free(buffer);
         buffer = NULL;     
      }
   }

   // clean up
   fclose(file);
   file = NULL;
  
   return(total_cores);
}

void
clear_ptr(char * ptr)
{
   free(ptr);
   ptr = NULL;
}

void
clear_multi_ptr(int a, ...)
{
   va_list ptr;
   va_start(ptr, a);
   for (int i = 0; i < a; i++) {
      char * local = va_arg(ptr, char *);
      if (local) {
         free(local);
         local = NULL;
      }
   }
   va_end(ptr);
}

long int
string_multi_len(int a, ...)
{
   long int retval = 0;
   va_list ptr;
   va_start(ptr, a);
   for (int i = 0; i < a; i++) {
      char * local = va_arg(ptr, char *);
      if (local) {
         retval += strlen(local);
      }
   }
   va_end(ptr);

   return(retval);
}

CMPD_INTEREST
compounded_interest(float principle,
                    float interest,
                    int numpyr,
                    int nummon)
{
   // calculate it out
   // percent per compound numpyr
   float percpperiod = 1 + (interest / 100) / numpyr;
   // total compounds
   int totalcomps = numpyr * (nummon / 12);

   // compile total
   float total = principle * pow(percpperiod,
                                 totalcomps);

   // create, load, and return struct
   CMPD_INTEREST retval;
   retval.nummon = nummon;
   retval.compound = numpyr * (nummon / 12);
   retval.principal = principle;
   retval.payment = total / nummon;
   retval.totalinterest = (total *
                           principle) -
                           principle;
   retval.total = total;
    
   return(retval); 
}
