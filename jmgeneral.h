﻿/*
Copyright <2024> <Jason Gibson>

<BSD License 2.0>

Redistribution and use in source and binary forms, 
with or without modification, are permitted provided 
that the following conditions are met:

1. Redistributions of source code must retain the above 
copyright notice, this list of conditions and the 
following disclaimer. 

2. Redistributions in binary form must reproduce the 
above copyright notice, this list of conditions and 
the following disclaimer in the documentation and/or 
other materials provided with the distribution. 

3. Neither the name of the copyright 
holder nor the names of its contributors may be used 
to endorse or promote products derived from this 
software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS 
AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED 
WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE 
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR 
A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL 
THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY 
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR 
CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, 
PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF 
USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) 
HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER
IN CONTRACT, STRICT LIABILITY, OR TORT 
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY 
OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF 
THE POSSIBILITY OF SUCH DAMAGE.
*/

#ifndef LIBJMGENERAL
#define LIBJMGENERAL

#include <stdbool.h>
#include <stdio.h>
#define MAX_BUFFER 1024
#define RETURN_BUFFER MAX_BUFFER - 1

// lib-jmgeneral

// pb
int 
clear_stdout(void);

// pb
char * 
prompt_input_getline(const char * restrict prompt);

// pb
void 
prompt_print(const char * restrict prompt);

// pb
bool 
valid_input(char * restrict str);

// pb
char * 
home_file_paths(void);

void
ret_nano_secs(void * arg);

// pb
int
random_gen(const int high);

void 
char_flip(char * restrict a,
          char * restrict b);

void 
int_flip(int * restrict a,
         int * restrict b);

// pb
void 
prompt_fail(const char * restrict fail,
            const char * restrict func,
            const int error);

// pb
int 
number_of_digits(long int a);

// pb
int 
binary_to_decimal(char * num);

// pb
char * 
decimal_to_binary(long long int num);

// pb
char * 
current_time(void);

bool 
file_create_refresh(FILE ** file,
                    char * name,
                    bool fresh);

bool 
file_close_refresh(FILE ** file,
                   char * name,
                   bool fresh);

// pb
int 
core_count();

// pb
void 
clear_ptr(char * ptr);

void
clear_multi_ptr(int a,
                ...);

long int
string_multi_len(int a,
                 ...);

typedef struct comp_interest {
    int nummon; // total number of months calculated
    int compound; // number of times compounded
    float totalinterest; // total interest amount
    float principal; // principal amount
    float payment; // expected payment
    float total; // total interest + principal
} CMPD_INTEREST;

CMPD_INTEREST
compounded_interest(float principle,
                    float interest,
                    int numpyr,
                    int nummon);

// lib-jmstring

// pb
bool 
string_has_digits(const char * restrict str);

// pb
bool 
string_has_alpha(const char * restrict str);

// pb
bool 
string_has_punct(const char * restrict str);

// pb
bool 
string_has_space(const char * restrict str);


void 
string_flipper(char * restrict str);

// pb
bool 
string_valid_dollar(const char * restrict dollar);

// pb
char *
string_field_search(const char * restrict str,
                    const char * restrict delimiter,
                    const int field);

// lib-jmfileprocess

char * 
file_search_func(FILE * file,
                 const char * restrict searchstr,
                 const char * restrict delimiter,
                 const int field);

int 
file_num_lines(FILE * file);


void 
file_reverse_output(FILE * file,
                    FILE ** newfile);

int 
highest_integer_column(FILE * restrict file,
                       const char * restrict delimiter,
                       const int field);

void 
file_null_check(FILE * restrict file);

char * 
getdelim_mem_alloc(FILE * restrict file,
                   const char * restrict delimiter);

char * 
getline_mem_alloc(FILE * restrict file);


// pb
char * 
getline_stdin_mem_alloc(void);

long int 
file_longest_string(FILE * file,
                    const long int lines);


// lib-jmcards.c

char * 
card_return(void);

char * 
suit_return(void);

char * 
draw_card(char ** cards_drawn,
          const char * target,
          int counter);

char *
card_value_calc(const char * restrict card);

#endif // libjmgeneral
