﻿/*
Copyright <2024> <Jason Gibson>

<BSD License 2.0>

Redistribution and use in source and binary forms, 
with or without modification, are permitted provided 
that the following conditions are met:

1. Redistributions of source code must retain the above 
copyright notice, this list of conditions and the 
following disclaimer. 

2. Redistributions in binary form must reproduce the 
above copyright notice, this list of conditions and 
the following disclaimer in the documentation and/or 
other materials provided with the distribution. 

3. Neither the name of the copyright 
holder nor the names of its contributors may be used 
to endorse or promote products derived from this 
software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS 
AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED 
WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE 
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR 
A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL 
THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY 
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR 
CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, 
PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF 
USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) 
HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER
IN CONTRACT, STRICT LIABILITY, OR TORT 
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY 
OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF 
THE POSSIBILITY OF SUCH DAMAGE.
*/


#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <assert.h>
#include <sys/types.h>

#include "jmgeneral.h"

#ifdef _WIN32
#include "WINHELP/include/bsdgetline.h"
#endif

/*
  USAGE :

  char * s = file_search_func(temp, "test", ",", 1);
  // do stuff with s
  free(retval);

  ARGS :
  file = file to process
  searchstr = pattern to match
  delimiter = self explanatory
  field = example: field = 3 of "fu,bar,why,how" assuming ',' for 
  delimiter will return "how"
*/

char *
file_search_func(FILE * file,
                 const char * searchstr,
                 const char * delimiter,
                 int field)
{
   assert(file);
   assert(searchstr);
   assert(delimiter);
  
   // declare and initialize
   char * buffer = NULL;
   char * retval = NULL;

   // loop until find identifier in string. typically first field
   // tokenize once found
   while (retval == NULL) {
      buffer = getdelim_mem_alloc(file,
                                  delimiter);
      if (strstr(buffer,
                 searchstr)) {
         retval = string_field_search(buffer,
                                      delimiter,
                                      field);
      }
      free(buffer);
      buffer = NULL;
   }

   // clean up & return
   rewind(file);
   return(retval);
}

/*
  ARGS:
  file = file to count lines
*/

int
file_num_lines(FILE * file) // working
{
   // declare and initialize
   int a = 0;
   int b = 0;

   // loop through counting number of lines
   while ((b = fgetc(file)) != EOF) {
      if  (b == '\n') {
         a++;
      }
   }

   // clean up & return
   rewind(file);
   return(a);
}

/*
  This will flip the contents of a given file and output to either 
  stdout or to a new file
  ARGS:
  file = original source file
  newfile = output point (typically temp file of sorts?). NULL pointer 
  will print to stdout
*/

void
file_reverse_output(FILE * file,
                    FILE ** newfile)
{
   // don't assert newfile as NULL is valid
   assert(file);
 
   // declare & initialize
   long int len = file_num_lines(file) + 1;
   char ** array = calloc(len,
                          sizeof(char*));

   // loop through each line in text file, put in buffer for
   // future stdout / fprintf
   for (int i = 0; i < len; i++) {
      array[i] = getline_mem_alloc(file);
   }

   // reset file for next usage
   rewind(file);

   // print buffer in reverse order to either stdout or newfile
   for (int i = (len - 1); i >= 0; i--) {
      if (newfile) {
         fprintf(*newfile,
                 "%s",
                 array[i]);
      } else {
         printf("%s",
                array[i]);
      }
      free(array[i]);
   }
   free(array);
}

/*
 returns allocated memory from file line via getline. wrapped in 
 function for easy reuse.
 ARGS:
 file = file to read
 delimiter = int form of delimiter in file. called from other 
 functions via atoi(delimiter)
*/

char *
getdelim_mem_alloc(FILE * file,
                   const char * delimiter)
{
   assert(file);
   assert(delimiter);
  
   // declare & initialize
   char * buffer = NULL;
   size_t buflen = 0;
  
   // get delimited line from file till null
   if (getdelim(&buffer,
                &buflen,
                atoi(delimiter),
                file) == -1) {
      free(buffer);
      buffer = NULL;
   }

   return(buffer);
}

/*
  extracts line from file via getline
  thank you to NevemTeve & astrogeek @
  linuxquestions.org for helping nail down
  valgrind error on test. change applied to other
  getline functions in this library.

  ARGS:
  file = file to pull line from
*/

char *
getline_mem_alloc(FILE * file)
{
   assert(file);

  // declare & initialize
   char * buffer = NULL;
   size_t buflen = 0;
  
  // get full line from file till null
   if (getline(&buffer,
               &buflen,
               file) == -1) {
      free(buffer);
      buffer = NULL;
   }
  
   return(buffer);
}

/*
  finds the longest string in a given file.
  the purpose of this function is fighting memory
  constraints. in functions such as the above file_reverse_output
  you can declare the local array to be exactly the size needed
  to hold up to the longest string without reserving more
  memory than absolutely needed.
  ARGS:
  file = file to find longest string from
*/

long int
file_longest_string(FILE * file,
                    long int lines)
{
   assert(file);
  
   // declare & initialize
   long int len = 0;

   // loop through each line in file.
   // set return value to longest string in file
   for (int i = 0; i < lines; i++) {
      char * ptr = getline_mem_alloc(file);
      long int j = strlen(ptr);
      if (j > len) {
         len = j;
      }
      free(ptr); 
      ptr = NULL;
   }

   // clean up & return
   rewind(file);
   return(len + 1);
}
