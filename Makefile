#Copyright <2023> <Jason Gibson>

#<BSD License 2.0>

#Redistribution and use in source and binary forms,
#with or without modification, are permitted provided
#that the following conditions are met:

#1. Redistributions of source code must retain the above
#copyright notice, this list of conditions and the
#following disclaimer.

#2. Redistributions in binary form must reproduce the
#above copyright notice, this list of conditions and
#the following disclaimer in the documentation and/or
#other materials provided with the distribution.

#3. Neither the name of the copyright
#holder nor the names of its contributors may be used
#to endorse or promote products derived from this
#software without specific prior written permission.

#THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS
#AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED
#WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
#IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
#A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL
#THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY
#DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
#CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
#PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
#USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
#HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER
#IN CONTRACT, STRICT LIABILITY, OR TORT
#(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
#OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF
#THE POSSIBILITY OF SUCH DAMAGE.

# default target

.DEFAULT_GOAL := build

# variables
CC = gcc
CFLAGS = -O2 -g
SFLAGS = -static -static-libgcc
TESTCFLAGS = -Wredundant-decls -Wfloat-equal -Winline -Wunreachable-code -pedantic -Wextra -Wall -g

# files to work with
FILELIST = libjmgeneral.c libjmstring.c libjmfileprocess.c libjmcards.c
TESTFILES = tests/tests.c tests/cardtest.c tests/generaltests.c
OBJFILES = libjmgeneral.o libjmstring.o libjmfileprocess.o libjmcards.o
TESTOBJ = tests.o cardtest.o generaltests.o

# external libraries to link
NIXLIBLINK = -lm -lsodium

# target locations and file names
TAR = /usr/local
LIB = ${TAR}/lib
INC = ${TAR}/include
SOB = libjmgeneral
HDR = jmgeneral.h

build: # shared library build
	@clear
	@echo
	@echo "compiling and building shared object file"
	@${CC} -c ${CFLAGS} -fPIC ${FILELIST}
	@${CC} -shared -o ${SOB}.so ${OBJFILES} ${NIXLIBLINK}
	@echo "shared object file built"
	@echo

static: # static library build
	@clear
	@echo
	@echo "compiling and building library for static linking"
	@${CC} -c ${CFLAGS} ${SFLAGS} ${FILELIST}
	@ar rcs ${SOB}.a ${OBJFILES}
	@echo "static library built"
	@echo

test: # static library with all tests and build tests enabled
	@clear
	@echo
	@echo "compiling and building library for test mode on *nix hosts"
	@${CC} ${TESTCFLAGS} ${FILELIST} ${TESTFILES} ${NIXLIBLINK} -o ${SOB}.test
	@echo
	@echo "testing executable ${SOB}.test built. running test"
	@echo
	@./${SOB}.test
	@cat /tmp/jmtestlog.log && rm /tmp/jmtestlog.log
	@echo
	@echo "test run complete"
	@echo

install: # install to target
	@clear
	@echo
	@if [ -f ${LIB}/${SOB}.so ] ; then rm ${LIB}/${SOB}.so ; fi
	@if [ -f ${INC}/${HDR} ] ; then rm ${INC}/${HDR} ; fi
	@if [ ! -d ${INC} ] ; then install -d ${INC} ; chmod 775 ${INC} ; fi
	@install -m 644 ${HDR} ${INC}/ ; echo "installed header file"
	@if [ ! -d ${LIB} ] ; then install -d ${LIB} ; chmod 775 ${LIB} ; fi
	@install -m 644 ${SOB}.so ${LIB}/ ; echo "installed shared object"
	@$(whereis ldconfig | awk '{print $2}')
	@echo
	@echo "you may need to export LD_LIBRARY_PATH=\"${LIB}\" via bashrc or profile"
	@echo

uninstall: # uninstall from target
	@clear
	@echo
	@if [ -f ${INC}/${HDR} ] ; then rm ${INC}/${HDR} ; fi
	@if [ -f ${LIB}/${SOB}.so ] ; then rm ${LIB}/${SOB}.so ; fi
	@echo
	@echo "installed files removed"
	@echo

clean: # eliminate stray files created during build process / testing files to clean directory
	@clear
	@if [ -f ${SOB}.a ] ; then rm ${SOB}.a ; fi
	@if [ -f ${SOB}.so ] ; then rm ${SOB}.so ; fi
	@if [ -f ${SOB}.o ] ; then rm *.o ; fi
	@if [ -f ${SOB}.test ] ; then rm ${SOB}.test ; fi
	@if [ -f jmtestlog.log ] ; then rm jmtestlog.log ; fi
	@if [ -f gmon.out ] ; then rm gmon.out ; fi
	@echo
	@echo "directory cleaned for next compilation"
	@echo
