#!/usr/bin/python3
# python bindings for libjmgeneral.so

# Copyright <2024> <Jason Gibson>

# <BSD License 2.0>

# Redistribution and use in source and binary forms, 
# with or without modification, are permitted provided 
# that the following conditions are met:

# 1. Redistributions of source code must retain the above 
# copyright notice, this list of conditions and the 
# following disclaimer. 

# 2. Redistributions in binary form must reproduce the 
# above copyright notice, this list of conditions and 
# the following disclaimer in the documentation and/or 
# other materials provided with the distribution. 

# 3. Neither the name of the copyright 
# holder nor the names of its contributors may be used 
# to endorse or promote products derived from this 
# software without specific prior written permission.

# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS 
# AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED 
# WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE 
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR 
# A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL 
# THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY 
# DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR 
# CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, 
# PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF 
# USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) 
# HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER
# IN CONTRACT, STRICT LIABILITY, OR TORT 
# (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY 
# OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF 
# THE POSSIBILITY OF SUCH DAMAGE.

import ctypes as ct
from ctypes import *
import pathlib

lib = pathlib.Path().absolute() / "/usr/local/lib/libjmgeneral.so"
local_lib = ct.CDLL(lib)

# return a random integer between a and b
def random_gen(a):
	return local_lib.random_gen(a)

# free a char pointer as needed
def clear_ptr(a):
  localbin = local_lib.clear_ptr
  localbin.argtypes = ct.c_void_p,
  localbin.restype = None
  localbin(a)

# print a prompt and return input
def prompt_input_getline(a):
  # prompt return
  localbin = local_lib.prompt_input_getline
  localbin.argtypes = [c_char_p]
  localbin.restype = ct.POINTER(ct.c_char)
  # execute & return
  localret = localbin(a.encode('utf-8'))
  retval = ct.cast(localret, ct.c_char_p).value
  clear_ptr(localret)
  return(retval.decode('utf-8'))

# return /home dir for user
def home_file_paths():
  localbin = local_lib.home_file_paths
  localbin.argtypes = None
  localbin.restype = ct.POINTER(ct.c_char)
  # execute & return
  localret = localbin()
  retval = ct.cast(localret, ct.c_char_p).value
  clear_ptr(localret)
  return(retval.decode('utf-8'))

# print current time
def current_time():
  localbin = local_lib.current_time
  localbin.argtypes = None
  localbin.restype = ct.POINTER(ct.c_char)
  # execute & return
  localret = localbin()
  retval = ct.cast(localret, ct.c_char_p).value
  clear_ptr(localret)
  return(retval.decode('utf-8'))

# clear terminal
def clear_stdout():
  local_lib.clear_stdout

# valid input check
def valid_input(a):
  localbin = local_lib.valid_input
  localbin.argtypes = [c_char_p]
  localbin.restype = ct.c_bool
  # return 1 if true, 0 if false
  return localbin(a.encode('utf-8'))

def binary_to_decimal(a):
  localbin = local_lib.binary_to_decimal
  localbin.argtype = [c_char_p]
  localbin.restype = ct.c_int
  # execute & return
  return localbin(a.encode('utf-8'))

def decimal_to_binary(a):
  localbin = local_lib.decimal_to_binary
  localbin.argtypes = [c_long]
  localbin.restype = ct.POINTER(ct.c_char)
  # execute & return
  localret = localbin(a)
  retval = ct.cast(localret, ct.c_char_p).value
  clear_ptr(localret)
  return(retval.decode('utf-8'))

def core_count():
  return local_lib.core_count()

def string_has_digits(a):
  localbin = local_lib.string_has_digits
  localbin.argtype = [c_char_p]
  localbin.restype = ct.c_bool
  return localbin(a.encode('utf-8'))

def string_has_alpha(a):
  localbin = local_lib.string_has_alpha
  localbin.argtype = [c_char_p]
  localbin.restype = ct.c_bool
  return localbin(a.encode('utf-8'))

def string_has_punct(a):
  localbin = local_lib.string_has_punct
  localbin.argtype = [c_char_p]
  localbin.restype = ct.c_bool
  return localbin(a.encode('utf-8'))

def string_has_space(a):
  localbin = local_lib.string_has_space
  localbin.argtype = [c_char_p]
  localbin.restype = ct.c_bool
  return localbin(a.encode('utf-8'))

def string_field_search(a, b, c):
  localbin = local_lib.string_field_search
  localbin.argtypes = [c_char_p, c_char_p, c_int]
  localbin.restype = ct.POINTER(ct.c_char)
  # execute & return
  localret = localbin(a.encode('utf-8'), b.encode('utf-8'), c)
  retval = ct.cast(localret, ct.c_char_p).value
  clear_ptr(localret)
  return(retval.decode('utf-8'))

def getline_stdin_mem_alloc():
  localbin = local_lib.getline_stdin_mem_alloc
  localbin.argtypes = None
  localbin.restype = ct.POINTER(ct.c_char)
  #execute & return
  localret = localbin()
  retval = ct.cast(localret, ct.c_char_p).value
  clear_ptr(localret)
  return(retval.decode('utf-8'))

def prompt_print(a):
  localbin = local_lib.prompt_print
  localbin.argtypes = [c_char_p]
  localbin.restype = None
  localbin(a.encode('utf-8'))

def number_of_digits(a):
  localbin = local_lib.number_of_digits
  localbin.argtypes = [c_long]
  localbin.restype = c_int
  return(localbin(a))

def prompt_fail(a, b, c):
  localbin = local_lib.prompt_fail
  # args failure - function - code to print
  localbin.argtypes = [c_char_p, c_char_p, c_int]
  localbin.restype = None
  localbin(a.encode('utf-8'), b.encode('utf-8'), c)

def string_valid_dollar(a):
  localbin = local_lib.string_valid_dollar
  localbin.argtypes = [c_char_p]
  localbin.restype = c_bool
  return localbin(a.encode('utf-8'))
