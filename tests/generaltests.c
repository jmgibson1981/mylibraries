// libjmgeneral.c tests
#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <string.h>
#include "tests.h"

#include "../jmgeneral.h"

void test_random_gen()
{
  FILE * log = NULL;
  FILE ** plog = &log;
  file_create_refresh(plog,
                      TESTFILE,
                      false);
  int retval = 0;
  int totaltests = 0;
  for (int i = 0; i < 5; i++) {
    int test = random_gen(100);
    if (test > 100) {
      retval = i + 1;
//      break;
    }
    totaltests++;
  }
  if (retval == 0) {
    test_result(plog,
                "random_gen",
                totaltests,
                true);
  } else {
    test_result(plog,
                "random_gen",
                retval,
                false);
  }
  file_close_refresh(plog,
                     TESTFILE, 
                     false);
}

void test_number_of_digits()
{
  FILE * log = NULL;
  FILE ** plog = &log;
  file_create_refresh(plog,
                      TESTFILE,
                      false);
  int retval = 0;
  int tests = 0;

  if (number_of_digits(300) == 3) {
    tests++;
  } else {
    retval = 1;
    goto test_number_of_digits_fail;
  }

  if (number_of_digits(11293478) == 8) {
    tests++;
  } else {
    retval = 2;
    goto test_number_of_digits_fail;
  }

  if (number_of_digits(12992) == 5) {
    tests++;
  } else {
    retval = 3;
    goto test_number_of_digits_fail;
  }

  test_number_of_digits_fail:
  if (retval == 0 && tests == 3) {
    test_result(plog,
                "number_of_digits",
                tests,
                true);
  } else {
    test_result(plog,
                "number_of_digits",
                retval,
                false);
  }
  file_close_refresh(plog,
                     TESTFILE,
                     false);
}

void test_binary_to_decimal()
{
  FILE * log = NULL;
  FILE ** plog = &log;
  file_create_refresh(plog,
                      TESTFILE,
                      false);
  int retval = 0;
  int tests = 0;

  int buffer = binary_to_decimal("11101010");
  if (buffer == 234) {
    tests++;
  } else {
    retval = 1;
    goto test_binary_to_decimal_fail;
  }

  buffer = binary_to_decimal("01111011");
  if (buffer == 123) {
    tests++;
  } else {
    retval = 2;
    goto test_binary_to_decimal_fail;
  }

  buffer = binary_to_decimal("00011011");
  if (buffer == 27) {
    tests++;
  } else {
    retval = 3;
    goto test_binary_to_decimal_fail;
  }

  test_binary_to_decimal_fail:
  if (retval == 0 &&
      tests == 3) {
    test_result(plog,
                "binary_to_decimal",
                tests,
                true);
  } else {
    test_result(plog,
                "binary_to_decimal",
                retval,
                false);
  }
  file_close_refresh(plog,
                     TESTFILE,
                     false);
}

void test_decimal_to_binary()
{
  FILE * log = NULL;
  FILE ** plog = &log;
  file_create_refresh(plog,
                      TESTFILE,
                      false);
  int retval = 0;
  int tests = 0;

  char * buffer = NULL;
  buffer = decimal_to_binary(234);
  if (strcmp(buffer,
             "11101010") == 0) {
    tests++;
  } else {
    retval = 3;
    goto test_decimal_to_binary_fail;
  }
  
  free(buffer);
  buffer = NULL;

  buffer = decimal_to_binary(27);
  if (strcmp(buffer,
             "11011") == 0) {
    tests++;
  } else {
    retval = 3;
    goto test_decimal_to_binary_fail;
  }

  free(buffer);
  buffer = NULL;

  buffer = decimal_to_binary(123);
  if (strcmp(buffer,
             "1111011") == 0) {
    tests++;
  } else {
    retval = 3;
    goto test_decimal_to_binary_fail;
  }

  test_decimal_to_binary_fail:
  free(buffer);
  buffer = NULL;
  if (retval == 0 &&
      tests == 3) {
    test_result(plog,
                "decimal_to_binary",
                tests,
                true);
  } else {
    test_result(plog,
                "decimal_to_binary",
                retval, 
                false);
  }
  file_close_refresh(plog,
                     TESTFILE,
                     false);
}