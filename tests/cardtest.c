/*
Copyright <2023> <Jason Gibson>

Permission is hereby granted, free of charge, to any person
obtaining a copy of this software and associated documentation
files (the “Software”), to deal in the Software without
restriction, including without limitation the rights to use,
copy, modify, merge, publish, distribute, sublicense, and/or
sell copies of the Software, and to permit persons to whom
the Software is furnished to do so, subject to the following
conditions:

The above copyright notice and this permission notice shall
be included in all copies or substantial portions of the
Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
OTHER DEALINGS IN THE SOFTWARE.
*/

#include <stdio.h>
#include <stdbool.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>
#include "../jmgeneral.h"
#include "tests.h"

#ifndef CARDSTEST
#define FAILLOG "tests/test.draw.card"
#define FAILFILE "tests/test.draw.card.fails"
#endif

void test_draw_card(int iterations)
{
  // declare & initialize
  FILE * log = NULL;
  FILE ** plog = &log;
  file_create_refresh(plog,
                      FAILLOG,
                      true);
  int failed = 0;
  
  // start logfile with time
  char * curtime = current_time();
  fprintf(log,
          "\n\nstarting draw_card test log\n%s\n",
          curtime); 
  free(curtime);
  curtime = NULL;
  
  // total running number to calc full success
  int total_tests = 0;
  
  // loop through iterations to repeated testing
  for (int a = 0; a < iterations; a++) {
    
    // declare local variables
    char ** array = calloc(52, sizeof(char*));
    int diamonds = 0;
    int hearts = 0;
    int spades = 0;
    int clubs = 0;
    
    // loop through total deck 52. increment for each suit found
    for (int i = 0; i < 52;) {
      char * draw = draw_card(array,
                              "player",
                              i);
      if (draw) {
        if (strstr(draw,
                   "diamonds")) {
          diamonds++;
        } else if (strstr(draw,
                   "hearts")) {
          hearts++;
        } else if (strstr(draw,
                   "spades")) {
          spades++;
        } else {
          clubs++;
        }
        free(draw);
        draw = NULL;
        i++;
      }
    }
    
    // clear array memory used
    for (int i = 0; i < 52; i++) {
      free(array[i]);
      array[i] = NULL;
    }
    free(array);
    array = NULL;
    
    // a proper deck has 13 of each suit. if any other value comes up
    // test fails
    if (diamonds == 13 && 
        hearts == 13 &&
        spades == 13 &&
        clubs == 13) {
      total_tests = a;
      fprintf(log, "\ntest succeeded iteration %d", a + 1);
    } else {
      fprintf(log, "\ntest failed on iteration %d", a + 1);
      FILE * failfile = NULL;
      FILE ** pfailfile = &failfile;
      file_create_refresh(pfailfile,
                          FAILFILE,
                          false);
      fprintf(*pfailfile,
              "\nfailure on line %d\n of iteration %d\n",
              failed, 
              a + 1);
      failed++;
      file_close_refresh(pfailfile,
                         FAILFILE,
                         false);
    }
  }
  
  // close logfile
  fprintf(log, "\nend log\n");
  file_close_refresh(plog,
                     FAILLOG,
                     false);
  
  // verify successful number of iteratations vs called for iterations
  // iterations - 1 because loop starts at 0
  if (total_tests == iterations - 1 ) {
    printf("draw_card - %d tests correct\n",
           iterations);
  } else {
    printf("draw_card test failed\n");
  }
}
