/*
Copyright <2024> <Jason Gibson>

<BSD License 2.0>

Redistribution and use in source and binary forms, 
with or without modification, are permitted provided 
that the following conditions are met:

1. Redistributions of source code must retain the above 
copyright notice, this list of conditions and the 
following disclaimer. 

2. Redistributions in binary form must reproduce the 
above copyright notice, this list of conditions and 
the following disclaimer in the documentation and/or 
other materials provided with the distribution. 

3. Neither the name of the copyright 
holder nor the names of its contributors may be used 
to endorse or promote products derived from this 
software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS 
AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED 
WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE 
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR 
A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL 
THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY 
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR 
CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, 
PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF 
USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) 
HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER
IN CONTRACT, STRICT LIABILITY, OR TORT 
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY 
OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF 
THE POSSIBILITY OF SUCH DAMAGE.
*/

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <stdbool.h>
#include <assert.h>

#include "jmgeneral.h"

// functions for card games

/*
 * returns the proper name of a given card. King, Queen, Jack, Ace
 * this works by number in suit of deck. 13 cards per suit.
 * 1 / 11 = ace
 * 12 = jack
 * 13 = queen
 * 14 = king
*/

char * 
card_return(void)
{
   // declare & initialize
   int card = 0;
   
   while (card == 0) {
      card = random_gen(14);
   }
   char * retval = NULL;

   // set string to card number if named
   if (card == 1 ||
       card == 11) {
       retval = strdup("ace");
   } else if (card == 12) {
       retval = strdup("jack");
   } else if (card == 13) {
       retval = strdup("queen");
   } else if (card == 14) {
       retval = strdup("king");
   } else {
       retval = (char*)malloc(3 * sizeof(char));
       snprintf(retval, 
                2, 
                "%d", 
                card);
   }
  
   return(retval);
}

char * 
suit_return(void)
{
   // declare & initialize
   int suit = 0;

   while (suit == 0) {
      suit = random_gen(4);
   }

   char * retval = NULL;

   if (suit == 1) {
      retval = strdup("clubs");
   } else if (suit == 2) {
      retval = strdup("diamonds");
   } else if (suit == 3) {
      retval = strdup("hearts");
   } else {
      retval = strdup("spades");
   }

   return(retval);
}

// returns a card from the deck!
/*
sample call.

void * load_arrays(void * target) {
  char * localtar = (char*)target;
  char ** deck = malloc(52 * sizeof(char*));
  
  for (int i = 0; i < 52;) {
    char * card = draw_card(deck, target, i);
    if (card) {
      i++;
      printf("%s\n", card);
      free(card);
      card = NULL;
    }
  }

  // clean up & return
  for (int j = 0; j < 52; j++) {
    free(deck[j]);
    deck[j] = NULL;
  }
  free(deck);
  deck = NULL;
  localtar = NULL;
  
  return(NULL);
}
*/

char * 
draw_card(char ** deck, 
          const char * target,
          int counter)
{
   // declare & initialize
   char * cardret = card_return();
   char * suit = suit_return();
   char * value = card_value_calc(cardret);
   assert(cardret);
   assert(suit);
   assert(value);


   long int a = strlen(cardret) +
                strlen(suit) +
                strlen(target) +
                strlen(value) +
                12;
   char * retval = NULL;
   char * buffer = (char*)malloc(a * sizeof(char));

   // load values into string to check and return
   snprintf(buffer,
            a,
            "%s of %s to %s - %s",
            cardret,
            suit,
            target,
            value);
   // clean up pointers
   clear_multi_ptr(3, cardret, suit, value);

   // runs compare if at least one card loaded in array
   if (counter >= 1) {
      // loops through array checking each loaded element
      // clear pointer as needed
      for (int i = 0; i < counter; i++) {
         if (strcmp(buffer,
                    deck[i]) == 0) {
            free(buffer);
            buffer = NULL;
            break;
         }
      }
   }
  
   // if card not found in array or first iteration set card return value
   // and load array.

   if (buffer) {
      retval = strdup(buffer);
      deck[counter] = buffer;
   }

   return(retval);
}

// returns card values for blackjack and others

char *
card_value_calc(const char * card)
{
   assert(card);
   // declare & initialize
   char * retval = NULL;
   char * cardname = string_field_search(card,
                                         " ",
                                         0);
   // test string contents and set return as needed
   if (strcmp(cardname,
              "king") == 0 ||
       strcmp(cardname,
              "queen") == 0 ||
       strcmp(cardname,
              "jack") == 0) {
      retval = strdup("10");
   } else if (strcmp(cardname,
                     "ace") == 0) {
      retval = strdup("11");
   } else {
      retval = strdup(card);
   }

   // clean up & return
   free(cardname);
   cardname = NULL;
   return(retval);
}
