#!/bin/sh
# local script for faster rebuild instead of manually running make commands

LOCALDIR=mylibraries

if pwd | grep "$LOCALDIR" ; then
  make
  make install
  make clean
else
  echo "this script is for building the c mylibraries folder"
fi
