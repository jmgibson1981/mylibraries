/*
Copyright <2024> <Jason Gibson>

<BSD License 2.0>

Redistribution and use in source and binary forms, 
with or without modification, are permitted provided 
that the following conditions are met:

1. Redistributions of source code must retain the above 
copyright notice, this list of conditions and the 
following disclaimer. 

2. Redistributions in binary form must reproduce the 
above copyright notice, this list of conditions and 
the following disclaimer in the documentation and/or 
other materials provided with the distribution. 

3. Neither the name of the copyright 
holder nor the names of its contributors may be used 
to endorse or promote products derived from this 
software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS 
AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED 
WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE 
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR 
A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL 
THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY 
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR 
CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, 
PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF 
USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) 
HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER
IN CONTRACT, STRICT LIABILITY, OR TORT 
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY 
OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF 
THE POSSIBILITY OF SUCH DAMAGE.
*/

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <ctype.h>
#include <assert.h>

#include "jmgeneral.h"

/*
  This will determine if any characters in string are digits

  return true = digits found

  ARGS:
  str = string to check for digit characters
*/

bool
string_has_digits(const char * str)
{
   assert(str);
  
   // declare & initialize
   bool retval = false;

   // iterate through str, if digit found retval = true
   for (unsigned long int i = 0; i <= strlen(str); i++) {
      if (isdigit((unsigned int) str[i])) {
         retval = true;
         break;
      }
   }

   return(retval);
}

/*
  This will determine if any characters in string are alpha

  return true = alpha characters found (a-z)

  ARGS:
  str = string to check for alphabetic characters
*/

bool
string_has_alpha(const char * str)
{
   assert(str);
  
   // declare & initialize
   bool retval = false;

   // iterate through str, if alpha found retval = true
   for (unsigned long int i = 0; i <= strlen(str); i++) {
      if (isalpha((unsigned int) str[i])) {
         retval = true;
         break;
      }
    }

   return(retval);
}

/*
  This will determine if any characters in string are punctuation

  return true = punctuation found

  ARGS:
  str = string to check for punctuation characters
*/

bool
string_has_punct(const char * str)
{
   assert(str);
  
   // declare & initialize
   bool retval = false;

   // iterate through str, if punct found retval = true
   for (unsigned long int i = 0; i <= strlen(str); i++) {
      if (ispunct((unsigned int) str[i])) {
         retval = true;
         break;
      }
   }

   return(retval);
}

/*
  This will determine if any spaces are present


  return true = spaces found

  ARGS:
  str = string to check for spaces
*/

bool
string_has_space(const char * str)
{
   assert(str);
  
   // declare & initialize
   bool retval = false;

   // iterate through str, if space found retval = true
   for (unsigned long int i = 0; i <= strlen(str); i++) {
      if (strchr(str,
                 ' ')) {
         retval = true;
         break;
      }
   }

   return(retval);
}

/*
  Adjusted for my own use. original source for this found at
  https://www.javatpoint.com/reverse-a-string-in-c

  This will reverse a string

  ARGS:
  str = string to flip
*/

void
string_flipper(char * str)
{
   assert(str);
  
   // declare & initialize
   int strsize = strlen(str);
   int temp = 0;

   for (int i = 0; i < strsize/2; i++) {
      temp = str[i];
      str[i] = str[strsize - i - 1];
      str[strsize - i - 1] = temp;
   }
}

/*
  return true if dollar value is valid

  ARGS:
  dollar = us dollar value (100.32) / (100)
*/

bool
string_valid_dollar(const char * dollar)
{
   assert(dollar);
  
   // declare & initialize
   int len = strlen(dollar);
   int digits = 0;
   bool retval = false;
   char * buffer = NULL;

   // first test for alpha and spaces
   if (!string_has_alpha(dollar) &&
       !string_has_space(dollar)) {

     // test punctuation
      if (string_has_punct(dollar)) {

         // if punctuation found only continue if . character
         if (strchr(dollar,
                    '.')) {

            for (int i = 0; i <= len; i++) {
               if (isdigit((unsigned int) dollar[i])) {
                  digits++;
               }
            }

            // verify postition of . character
            if (digits == len - 1) {
               buffer = strdup(dollar);
               string_flipper(buffer);
               if (buffer[2] == '.') {
                  free(buffer); buffer = NULL;
                  retval = true;
                  goto BREAKOUT;
               }
            }
         }
      } else {
         retval = true;
      }
   }

   BREAKOUT:
   return(retval);
}

char *
string_field_search(const char * str,
                    const char * delimiter,
                    int field)
{
   assert(str); 
   assert(delimiter);
  
   // declare & initialize
   char * retval = NULL;
   char * token = NULL;
   int counter = 0;
   char * buffer = strdup(str);

   // tokenize and iterate till field found
   do {
      token = strtok(buffer,
                     delimiter);
      if (!token) {
         break;
      }
      while (token) {

         // if proper value iterate until field
         if (counter == field) {
            retval = strdup(token);
            assert(retval);
            retval[strcspn(retval,
                           "\n")] = '\0';
            token = NULL;
            break;
         } else {
        
         // else continue to next token until found
         counter++;
         token = strtok(NULL,
                        delimiter);
         }
      }
   } while (counter < field);

   // clean up and return
   free(buffer);
   buffer = NULL;
   return(retval);
}
